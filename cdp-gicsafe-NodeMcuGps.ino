/*======================================[GPS - NodeMCU]===========================================================
   Copyright Author Yoel López  <lopez.yoel25@gmail.com>
   All rights reserved.
   Version: 1.0.0
   Creation Date: 2019/06/18
*/
/*===========================[Avoid multiple inclusion - begin]===================================================*/

#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>

/*==================================[Definitions of public data types]===========================================*/

#define CONVERSION_KMH 1.60934
#define WS_PORT_NMCU 80

TinyGPSPlus gps;
SoftwareSerial ss(4, 5);

const char* ssid = "Fibertel WiFi419 2.4GHz";
const char* password = "0043527009";

float latitude , longitude;
double speed_dbl;
int year , month , date, hour , minute , second;
String date_str , time_str , lat_str , lng_str;
int pm;

WiFiServer server(WS_PORT_NMCU); /* Create Web Server with NodeMCU to publish data */

/*======================================[SETUP - Initialize Configurations]===========================================================*/
void setup()
{

  Serial.begin(115200);
  ss.begin(9600);
  
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());

}

/*======================================[LOOP - Prepare Data]=========================================================================*/

void loop()
{

  while (ss.available() > 0)


    if (gps.encode(ss.read()))
    {
      if (gps.location.isValid())
      {
        latitude = gps.location.lat();
        lat_str = String(latitude , 6);
        longitude = gps.location.lng();
        lng_str = String(longitude , 6);
        speed_dbl = (gps.speed.mph()) * CONVERSION_KMH;

      }

      if (gps.date.isValid())
      {
        date_str = "";
        date = gps.date.day();
        month = gps.date.month();
        year = gps.date.year();

        if (date < 10)
          date_str = '0';
        date_str += String(date);

        date_str += " / ";

        if (month < 10)
          date_str += '0';
        date_str += String(month);

        date_str += " / ";

        if (year < 10)
          date_str += '0';
        date_str += String(year);
      }

      if (gps.time.isValid())
      {
        time_str = "";
        hour = gps.time.hour();
        minute = gps.time.minute();
        second = gps.time.second();

        //minute = (minute + 30);
        if (minute > 59)
        {
          minute = minute - 60;
          hour = hour + 1;
        }
        hour = (hour + 5) ;
        if (hour > 23)
          hour = hour - 24;

        if (hour >= 12)
          pm = 1;
        else
          pm = 0;

        hour = hour % 12;

        if (hour < 10)
          time_str = '0';
        time_str += String(hour);

        time_str += " : ";

        if (minute < 10)
          time_str += '0';
        time_str += String(minute);

        time_str += " : ";

        if (second < 10)
          time_str += '0';
        time_str += String(second);

        if (pm == 1)
          time_str += " PM ";
        else
          time_str += " AM ";

      }

    }

  WiFiClient client = server.available(); /*Check if a client has connected*/
  if (!client)
  {
    return;
  }

  /*======================================[Prepare the response to show data]===========================================================*/

  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n <!DOCTYPE html> <html> <head> <title>GPS Neo6M - NodeMCU</title> <style>";
  s += "a:link {background-color:BLUE;text-decoration: none;}";
  s += "table, th, td {border: 1px solid black;} </style> </head> <body> <h1  style=";
  s += "font-size:300%;";
  s += " ALIGN=CENTER>GPS Neo6M - NodeMCU</h1>";
  s += "<p ALIGN=CENTER style=""font-size:150%;""";
  s += "> <b>Datos de Geolocalizacion</b></p> <table ALIGN=CENTER style=";
  s += "width:50%";
  s += "> <tr> <th>Latitud</th>";
  s += "<td ALIGN=CENTER >";
  s += lat_str;
  s += "</td> </tr> <tr> <th>Longitud</th> <td ALIGN=CENTER >";
  s += lng_str;
  s += "</td> </tr> <tr>  <th>Fecha</th> <td ALIGN=CENTER >";
  s += date_str;
  s += "</td></tr> <tr> <th>Velocidad KM/H </th> <td ALIGN=CENTER >";
  s += speed_dbl;
  s += "</td>  </tr> </table> ";


  if (gps.location.isValid())
  {
    s += "<p align=center><a style=""color:RED;font-size:125%;"" href=""http://maps.google.com/maps?&z=15&mrt=yp&t=k&q=";
    s += lat_str;
    s += "+";
    s += lng_str;
    s += """ target=""_top"">Haga click aqui!</a> Chequear la localizacion en Google maps.</p>";
  }

  s += "</body> </html> \n";

  client.print(s);
  delay(100);

}
